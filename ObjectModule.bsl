﻿
Функция ПолучитьЗапросРегистраНакопления(ИмяРегистра, ИмяРесурса = Неопределено, Контрагент, ДоговорКонтрагента, СчетОплаты) Экспорт 
	
	МетаданныеРегистра = Метаданные.РегистрыНакопления[ИмяРегистра];
	ИмяРесурса = "";
	Если МетаданныеРегистра.Ресурсы.Найти("Количество") <> Неопределено Тогда
		
		ИмяРесурса = "Количество";
		
	ИначеЕсли МетаданныеРегистра.Ресурсы.Найти("Сумма") <> Неопределено Тогда 
		
		ИмяРесурса = "Сумма"; // нет

		
	Иначе 
		
		ИмяРесурса = МетаданныеРегистра.Ресурсы[0].Имя;
		Сообщить("Для регистра накопления """ + МетаданныеРегистра.Синоним + """ не обнаружен ресурс!!! Взят ресурс:""" + ИмяРесурса + """", СтатусСообщения.ОченьВажное);
		///eee
	КонецЕсли; 
	
	ТекстЗапроса = "";
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	ОстаточныйРегистр.*
	|ИЗ
	|	РегистрНакопления." + ИмяРегистра + ".Остатки(&ДатаОстатков,"; 
	Если НЕ Контрагент.Пустая() Тогда
		ТекстЗапроса = ТекстЗапроса + "Контрагент В (&Контрагент)";
		Если НЕ ДоговорКонтрагента.Пустая() ИЛИ  НЕ СчетОплаты.Пустая() Тогда
			ТекстЗапроса = ТекстЗапроса + "И";
		КонецЕсли;
	КонецЕсли;
	
	Если НЕ ДоговорКонтрагента.Пустая() Тогда
		ТекстЗапроса = ТекстЗапроса + "ДоговорКонтрагента В (&ДоговорКонтрагента)";
		Если НЕ СчетОплаты.Пустая()Тогда
			ТекстЗапроса = ТекстЗапроса + "И";
		КонецЕсли;
	КонецЕсли;
	
	Если НЕ СчетОплаты.Пустая() Тогда
		ТекстЗапроса = ТекстЗапроса + "СчетОплаты В (&СчетОплаты)) КАК ОстаточныйРегистр";
	Иначе	
		ТекстЗапроса = ТекстЗапроса + ") КАК ОстаточныйРегистр";
	КонецЕсли;
	
	Возврат ТекстЗапроса;
	
КонецФункции
